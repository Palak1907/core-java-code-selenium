package javadatatypes;

import java.util.Scanner;

/*Write a Java program to convert minutes into a number of years and days.
Test Data Input the number of minutes: 3456789 
Expected Output :3456789 minutes is approximately 6 years and 210 days*/
//1 hour = 60 min ,1 day = 24 hours, 1 year = 365 days
/*1 day  = 24*60 min = 1440 , 1 year = 365*24*60 = 525600 , 1 min = 1 year / 365*24*60 */
public class MinutesToYearsAndDays {

	public static void main(String[] args) {
		System.out.println("Enter the number of Minutes");
		Scanner sc = new Scanner(System.in);
		long minutes = sc.nextLong();
		
		long year = (long) (minutes/525600);
		System.out.println(year +" Year");
		int days = (int) (minutes / 1440) % 365; 
		//OR days = minutes % 525600 
		//days = days/1440
		System.out.println(days + " Days");
		System.out.println(minutes+" minutes is approximately "+year +" Years and "+ days +" days");
		sc.close();
	}

}
